/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/02 19:09:14 by mstygg            #+#    #+#             */
/*   Updated: 2018/12/02 19:20:37 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	ch;
	int		count;

	ch = c;
	if (ch == 0)
		return ((char*)(s + ft_strlen(s)));
	if (!s)
		return (NULL);
	count = ft_strlen(s);
	s += count - 1;
	while (count && *s != ch)
	{
		--s;
		--count;
	}
	return (count) ? ((char*)s) : (NULL);
}
