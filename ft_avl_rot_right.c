/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_avl_rot_right.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 00:59:26 by mstygg            #+#    #+#             */
/*   Updated: 2018/12/23 23:40:34 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

t_avl_t	*ft_avl_rot_right(t_avl_t *p)
{
	t_avl_t	*tmp;

	if (!p)
		return (NULL);
	tmp = p->left;
	p->left = tmp->right;
	tmp->right = p;
	ft_avl_fix_height(p);
	ft_avl_fix_height(tmp);
	return (tmp);
}
